.model small
.stack 256
.data
    endl db 13, 10, '$'
    minword db 0FFh
    temp dw ?
    len db 0
    string db 99, 100 dup('$')
    newString db 102, 103 dup('$')
    finalString db 99, 100 dup('$')
.code

DeleteMins proc
  push ax
  push bx
  push cx
  push dx

  xor ax, ax
  mov si, 2
  mov di, 2
  mov temp, si
  xor cx, cx

  jmp skipPoP
popSI:
;  pop ax
pushSI:
  cmp di, 2
  jz skipPush
  mov finalString[di], ' '
  inc di
skipPush:
  inc si
  mov temp, si
skipPoP:
  xor cx, cx
  mov ch, minword
check:
  mov bl, newString[si]
  cmp bl, ' '
  jnz nextletter
  cmp bl, 0dh
  jz eodelete

  cmp cl, ch
  jz popSI

  mov si, temp
  xor ch, ch
copy1:
  mov bl, newString[si]
  mov finalString[di], bl
  inc si
  inc di
  loop copy1
  cmp bl, 0dh;'$'
  jz eodelete
  jmp pushSI

nextletter:
  cmp bl,'$'; 0dh;'$'
  jz eodelete
  inc si
  inc cl
  jmp check

eodelete:
  pop dx
  pop cx
  pop bx
  pop ax
  ret
DeleteMins endp





main:
    mov ax, @data
    mov ds, ax
    mov es, ax

    xor ax, ax
    xor bx, bx
    xor cx, cx
    xor dx, dx


    mov ah,0ah
    lea dx,string
    int 21h

  ;  mov string+1,0ah

    mov ah,9h

    mov dx, offset endl
    int 21h
    lea dx,string + 2
    int 21h

    mov dx, offset endl
    int 21h
    int 21h
  ;  int 21h
    mov di, 1
    mov al, string[di]

    call CopyStr
    call CountStr
    call ScanWords
    call DeleteMins
  ;  mov dx, offset newString + 2
  ;  mov ah, 09h
  ;  int 21h
    mov dx, offset endl
    int 21h
    mov dx, offset finalString + 2
    int 21h
    mov dx, offset endl
    int 21h
    mov ah, len
    mov ah, minword

    mov ax, 4c00h
    int 21h

CountStr proc
    push ax
    push bx
    push cx
    push dx

    mov cx, 100
    mov dx, 100
    mov ax, offset newString + 2
    mov di, ax
    mov al, 13 ; код 1-й части enter

        repnz scasb
        sub dx, cx
        dec dx
        mov len, dl

    pop dx
    pop cx
    pop bx
    pop ax
    ret
CountStr endp

ScanWords proc
  push ax
  push bx
  push cx
  push dx

  mov cl, len
  mov ch, len
  mov ax, offset newString + 2
  mov di, ax
  mov al, 32
repeat:
  mov ch, cl
  repnz scasb
  cmp ch, cl
  ja worde
  jmp next

  worde:
    sub ch, cl
    dec ch
    mov bh, minword
    cmp ch, 1
    jz kostil2
    cmp ch, bh
    jnc repeat

    mov minword, ch

  cmp cl, 0
  jz next
  jmp repeat
  kostil2:
  mov minword, 0

next:
  mov ah, minword
  cmp ah, 0
  ja skipKostil
  mov minword, 1
skipKostil:
  pop dx
  pop cx
  pop bx
  pop ax
  ret
ScanWords endp

CopyStr proc
  push ax
  push bx
  push cx
  push dx

  xor si, si
  xor di, di
  mov bl, string[di]
  mov newString[si], bl
  inc si
  inc di
  mov bl, string[di]
  mov newString[si], bl
  inc si
  inc di
skipspace:
  mov bl, string[di]
  cmp bl, ' '
  jnz copyCycle
  inc di
  jmp skipspace
copyCycle:
  mov bl, string[di]
  cmp bl, '$'
  jne skipend
  jmp eocopy
skipend:
  cmp bl, ' '
  jne copy
  inc di
  mov bl, string[di]
  cmp bl, ' '
  jne notspace
  jmp copyCycle

notspace:
  dec di
  mov bl, string[di]
copy:
  mov newString[si], bl
  inc si
  inc di
  jmp copyCycle

eocopy:
  ;mov newString[si], ' '
  ;inc si
  ;mov newString[si], ' '
  ;inc si
  mov newString[si], 13
  inc si
  mov newString[si], 10

  pop dx
  pop cx
  pop bx
  pop ax
  ret
CopyStr endp



end main
