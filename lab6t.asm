model tiny
.code
.386
org 100h
main:
  mov si, 80h
  lodsb
  cmp al, 0
  je set_handler
  cmp al, 3
  jne input_error
  mov cx, 3
  mov di, offset param
  repe cmpsb
  jne input_error

  call remove_handler
  mov ah, 4ch
  int 21h

check_handler_inst:
  push ax bx es si

  xor ax, ax
  mov es, ax

  mov si, word ptr es:[09h * 4]   ;берем адрес обработчика
  mov bx, word ptr es:[09h * 4 + 2] ;и его сегмент
  mov es, bx
  cmp word ptr es:[si + 8], 2289h
  ;jne bad
  ;cmp bx, 61440
  ;jne bad
  jne good
  bad:
  mov ah, 09h
  mov dx, offset inst_err
  int 21h
  mov ah, 4ch
  int 21h
  good:
  mov ax, cs
  mov es, ax
  call out_uns
  mov ax, offset flag
  call out_uns
  pop si es bx ax
  ret

set_handler:
  push ax es
  call check_handler_inst
  xor ax, ax
  mov es, ax
  mov ax, word ptr es:[09h * 4]
  ;call out_uns
  mov word ptr old_handler, ax
  mov ax, word ptr es:[09h * 4 + 2]
  ;call out_uns
  mov word ptr old_handler + 2, ax

  mov ax, cs
  mov word ptr es:[09h * 4 + 2], ax
  mov ax, offset handler
  mov word ptr es:[09h * 4], ax
  pop es ax
  mov ah, 09h
  mov dx, offset good_inst
  int 21h
  mov dx, offset last_byte
  int 27h
  ret

input_error:
  mov ah, 09h
  mov dx, offset inp_err
  int 21h
  mov ah, 4ch
  int 21h
  ret

remove_handler:
  push dx ax bx es si
  call check_handler_del
  xor ax, ax
  mov es, ax

  mov ax, word ptr es:[09h * 4]   ;берем адрес нового обработчика
  mov bx, word ptr es:[09h * 4 + 2] ;и его сегмент
  mov es, bx
  mov si, ax
  ;xor ax, ax
  ;mov word ptr es:[si + 4], ax
  mov ax, word ptr es:[si]  ;берем первое слово по этому адресу(адрес старого обработчика)
  mov bx, word ptr es:[si + 2] ;берем второе слово по этому адресу(семент старого обработчика)
  push ax
  mov ax, bx
  pop ax
  push bx
  mov bx, 0     ;восстанавливаем адрес сегмента
  mov es, bx
  pop bx
  mov word ptr es:[09h * 4], ax   ;кидаем в адрес обработкика старый адрес
  mov word ptr es:[09h * 4 + 2], bx ;кидаем в адрес сегмента обработкика старый адрес
  mov ah, 09h
  mov dx, offset good_del
  int 21h

  pop si es bx ax dx
  ret

check_handler_del:
  push ax bx es si

  xor ax, ax
  mov es, ax

  mov si, word ptr es:[09h * 4]   ;берем адрес нового обработчика
  mov bx, word ptr es:[09h * 4 + 2] ;и его сегмент
  ;cmp ax, 59783
  ;je bad1
  ;cmp bx, 61440
  ;je bad1
  ;cmp word ptr es:[si], 2289h
  mov es, bx
  mov ax, es
  add si, 8
  call out_uns
  mov ax, si
  call out_uns
  mov ax, word ptr es:[si]
  cmp ax, 2289h
  je good1
  bad1:
  mov ah, 09h
  mov dx, offset rem_err
  int 21h
  mov ah, 4ch
  int 21h
  good1:
  pop si es bx ax
  ret

handler:
  old_handler dd 0, 0
  flag dw 2289h
  push es ds si di cx bx dx ax
  xor ax, ax
  mov ah, 02h
  int 16h
  cmp al, 0
  jne standart_handler	;если не специальная клавиша, переход на стандартный обработчик
	in al, 60h
  cmp al, 3bh	;если нажата клавиша "f1" то выводим на экран hello world
  jne @1
  mov si, offset help
  mov ax, offset help
  call print_to_buf
  jmp ok
  @1:
  cmp al, 3ch
  jne @2
  mov si, offset save
  mov ax, offset save
  call print_to_buf
  jmp ok
  @2:
  cmp al, 3dh
  jne @3
  mov si, offset open
  mov ax, offset open
  call print_to_buf
  jmp ok
  @3:
  cmp al, 3eh
  jne @4
  mov si, offset edit
  mov ax, offset edit
  call print_to_buf
  jmp ok
  @4:
  cmp al, 3fh
  jne standart_handler
  mov si, offset copy
  mov ax, offset copy
  call print_to_buf
  ok:
  mov ax, cs
  mov ds, ax
  mov ax, 0B801h
  mov es, ax
  xor di, di
  mov cx, msg_len
  rep movsb
end_handler:
  mov al, 20h
  out 20h, al
  pop ax dx bx cx di si ds es
  iret

standart_handler:
  pop ax dx bx cx di si ds es
  jmp dword ptr cs:old_handler
	iret

print_to_buf proc
  push ax es ds si di bx cx
  mov si, ax
  mov ax, 0040h
  mov es, ax
  mov ax, cs
  mov ds, ax
  mov di, word ptr es:[001ah]
  mov bx, word ptr es:[001ch]
  mov cx, 4
  lp:
    cmp di, 003ch
    jne g1
    mov di, 001eh
    g1:
      mov al, byte ptr ds:[si]

      mov byte ptr es:[di], al
      add si, 2
      add di, 2
      mov word ptr es:[001ch], di
      loop lp

  pop cx bx di si ds es ax
  ret
print_to_buf endp

out_uns proc
    push ax
    push cx
    push dx
    push bx
    xor cx, cx
    mov bx, 10

    get_number:
      xor dx, dx
      div bx
      add dl, '0'
      push dx
      inc cx
      test ax, ax
      jnz get_number
      mov ah, 02h

    output:
      pop dx
      int 21h
      loop output

    mov ah, 09h
    lea dx, endl
    int 21h
    pop bx
    pop dx
    pop cx
    pop ax
    ret
out_uns endp

  param db ' -d'
  good_inst db 'Installation completed', 10, 13, '$'
  good_del db 'Handler deleted', 10, 13, '$'
  rem_err db 'Handler does not exist', 10, 13, '$'
  inst_err db 'Handler is already installed', 10, 13, '$'
  inp_err db 'Bad command line parameter', 10, 13, '$'
  message db 'H', 0Ch, 'E', 0Ch, 'L', 0Ch, 'P', 0Ch
  help db 'H', 0Ch, 'E', 0Ch, 'L', 0Ch, 'P', 0Ch
  save db 'S', 0Ch, 'A', 0Ch, 'V', 0Ch, 'E', 0Ch
  open db 'O', 0Ch, 'P', 0Ch, 'E', 0Ch, 'N', 0Ch
  edit db 'E', 0Ch, 'D', 0Ch, 'I', 0Ch, 'T', 0Ch
  copy db 'C', 0Ch, 'O', 0Ch, 'P', 0Ch, 'Y', 0Ch
  endl db 10, 13, '$'
  msg_len dw 8
last_byte:
end main
