.model small
.stack 256
.data
    a dw 5
    len dw 15
    message db 'Hello world!', 13, 10, '$'
.code
main:
    mov ax, @data
    mov ds, ax
    mov es, ax
    mov ax, offset message
    mov di, ax

    xor ax, ax
    xor bx, bx
    xor cx, cx
    xor dx, dx

    mov cx, len
    mov al, 32
    repnz scasb    

    mov ax, 4c00h
    int 21h
end main
