.model small
.stack 256
.data
    row dw ?
    column dw ?
    count dw ?
    counter dw 2
    array dw 10*10 dup(?)
    oddFlag db ?
    signFlag db 0
    outputCounter dw 0
    skipper dw ?
    error db "incorrect number", 13, 10, '$'
    endl db 13, 10, '$'
    buff    db 7,7 Dup(?)
.code
main:
    mov ax, @data
    mov ds, ax

    mov ax, 0FFFFh
    mov bx, -1
    call SignAdd

    call InputInt
    mov row, ax
    call InputInt
    mov column, ax

    cmp row, 0
    jb wrongsize
    jz wrongsize
    cmp row, 10
    ja wrongsize

    cmp column, 0
    jb wrongsize
    jz wrongsize
    cmp column, 10
    ja wrongsize
    jmp rightsize

wrongsize:
    mov ax, 4c00h
    int 21h



rightsize:
    xor di, di
    mov ax, row
    mov bx, column
    mul bx

    mov count, ax
    mov cx, ax
inputArray:
    call InputInt
    mov array[di], ax
    add di, 2
    loop inputArray

    mov cx, count
    xor di, di
outputArray:
    mov ax, array[di]
    call OutInt
    mov ah, 02h
    mov dl, 9h
    int 21h

    add di, 2
    inc outputCounter
    push ax
    mov ax, outputCounter
    cmp ax, column
    pop ax
    jb NotNextColumn
    mov outputCounter, 0
    push ax
    push dx
    mov ah, 09h
    mov dx, offset endl
    int 21h
    pop dx
    pop ax
NotNextColumn:
    loop outputArray

    call ADDeven
    push ax
    mov ah, 09h
    mov dx, offset endl
    int 21h
    pop ax
    call OutInt
    mov ah, 09h
    mov dx, offset endl
    int 21h


    mov ax, 4c00h
    int 21h

MuLnum macro num
  push ax

  mov ax, num
  mov bx, 4
  mul bx
  mov bx, ax

  pop ax
 endm

CheckEnd proc
  push ax
  push dx

  mov ax, si
  mov dx, column
  div dx


  pop dx
  mov dl, al
  pop ax
  ret
CheckEnd endp

SignAdd proc
  push cx
  push dx

  add ax, 0
  jns positiveA
  mov signFlag, 1
  neg ax

positiveA:
  add bx, 0
  js negativeB
  jns positiveB

negativeB:
  neg bx
  add ax, bx
  jmp eoSignAdd

positiveB:
  add ax, bx
  jmp eoSignAdd

eoSignAdd:
  cmp signFlag, 0
  jz positiveAnswer
  neg ax
  mov signFlag, 0

positiveAnswer:
  pop dx
  pop cx
  ret
SignAdd endp

ADDeven proc
  push bx
  push cx
  push dx

  mov cx, count
  dec cx

  mov si, 2
  ;add si, 2

  mov ax, column ;чёт\нечёт столбцов
  mov bl, 2
  div bl
  MuLnum column ; mov bx, column * 4
  cmp ah, 0
  jnz odd
  mov skipper, 0
  xor ax, ax
  xor bx, bx
  mov oddFlag, 0
  jmp addnums

odd:
  mov skipper, 2
  mov oddFlag, 1
  xor ax, ax
  xor bx, bx

addnums:
;  push bx
;  mov bx, array[si]
;  call SignAdd
;  pop bx
  adc ax, array[si]
  jno continueAdding
  mov ah, 09h
  mov dx, offset endl
  int 21h
  mov dx, offset error
  int 21h
  mov ah, 04ch
  int 21h
continueAdding:
  add si, 4
  add counter, 4
  sub cx, 2
  jc eoaddnums

  push ax
  push bx
  mov ax, counter
  mov bx, column
  cmp ax, bx
  pop bx
  pop ax
  ja shift
  jmp addnums

shift:
  add si, skipper
  cmp oddFlag, 0
  jz NoNeedToSkip
  dec cx
  NoNeedToSkip:
  js eoaddnums
  jmp addnums


eoaddnums:
  pop dx
  pop cx
  pop bx
  ret
ADDeven endp

InputInt proc
    push bx
    push cx
    push dx
    push di

    mov ah, 0ah
    xor di, di
    mov dx, offset buff
    int 21h
    mov dl, 0ah
    mov ah, 02h
    int 21h

    mov si, offset buff + 2  ; берём адрес 1-го символа и проверяем на минус
    cmp byte ptr [si], '-'
    jnz ii1
    mov di,1
    inc si
ii1:
    xor ax,ax
    mov bx,10
ii2:
    mov cl, [si]             ; непосредственно ввод
    cmp cl, 0dh
    jz endin

    cmp cl, '0'
    jb er
    cmp cl, '9'
    ja er

    sub cl, '0'
    mul bx
    jo er
    add ax, cx
    inc si
    jmp ii2

er:
    mov dx, offset error
    mov ah, 09h
    int 21h
    mov ax, 4c00h
    int 21h


endin:
    cmp ax, 32768
    jnc er
    cmp di, 1
    jnz ii3
    neg ax
ii3:
    pop di
    pop dx
    pop cx
    pop bx
    ret
InputInt endp

OutInt proc
  push ax
  push bx
  push cx
  push dx


   test    ax, ax
   jns     oi1


   mov  cx, ax   ; смена знака
   mov     ah, 02h
   mov     dl, '-'
   int     21h
   mov  ax, cx
   neg     ax

oi1:
    xor     cx, cx
    mov     bx, 10
oi2:
    xor     dx, dx     ;запихиваем в стек
    div     bx

    push    dx
    inc     cx

    test    ax, ax
    jnz     oi2

    mov     ah, 02h
oi3:
    pop     dx        ;выводим из стека

    add     dl, '0'
    int     21h

    loop    oi3

  ;  mov ah, 09h
  ;  mov dx, offset endl
  ;  int 21h

    pop dx
    pop cx
    pop bx
    pop ax

    ret

OutInt endp

end main
