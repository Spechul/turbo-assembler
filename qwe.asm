code segment
assume cs:code, ds:code, es:code, ss:code
org 100h
Start:
  jmp SetMyHandler

MyIntHandler proc

old_handler dd 0,0
push es ds si di cx bx dx ax
xor ax, ax
mov ah, 02h
int 16h
cmp al, 0
jne standart_handler ;если не специальная клавиша, переход на стандартный обработчик
in al, 60h
cmp al, 3bh ;если нажата клавиша "f1" то выводим на экран hello world
jne @1
mov ax, cs
mov ds, ax
mov ax, 0B801h
mov es, ax
mov si, offset help
xor di, di
mov cx, msg_len
rep movsb
mov ax, offset help
call print_to_buf
jmp end_handler
@1:
cmp al, 3ch
jne @2
mov ax, cs
mov ds, ax
mov ax, 0B801h
mov es, ax
mov si, offset save
xor di, di
mov cx, msg_len
rep movsb
mov ax, offset save
call print_to_buf
jmp end_handler
@2:
cmp al, 3dh
jne @3
mov ax, cs
mov ds, ax
mov ax, 0B801h
mov es, ax
mov si, offset open
xor di, di
mov cx, msg_len
rep movsb
mov ax, offset open
call print_to_buf
jmp end_handler
@3:
cmp al, 3eh
jne @4
mov ax, cs
mov ds, ax
mov ax, 0B801h
mov es, ax
mov si, offset edit
xor di, di
mov cx, msg_len
rep movsb
mov ax, offset edit
call print_to_buf
jmp end_handler
@4:
cmp al, 3fh
jne standart_handler
mov ax, cs
mov ds, ax
mov ax, 0B801h
mov es, ax
mov si, offset copy
xor di, di
mov cx, msg_len
rep movsb
mov ax, offset copy
call print_to_buf
end_handler:
mov al, 20h ;яюёырть ёшуэры "ъюэхц яЁхЁытрэшя"
out 20h, al ; ъюэтЁюыыхЁу яЁхЁытрэшщ
pop ax dx bx cx di si ds es
iret

standart_handler:
  pop ax dx bx cx di si ds es
  jmp dword ptr cs:old_handler

  ;StandartOffset dd ?

MyIntHandler endp


SetMyHandler:
  mov ah, 35h
  mov al, 09h
  int 21h

  mov word ptr old_handler, bx
  mov word ptr old_handler + 2, es

  mov ax, 2509h
  mov dx, offset MyIntHandler
  int 21h

  mov dx, offset SetMyHandler
  int 27h

code ends
end Start
