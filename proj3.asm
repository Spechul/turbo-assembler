.model small
.stack 256
.data
    message db 'divide by zero', 13, 10, '$'
    error db "incorrect number", 13, 10, '$'
    endl db 13, 10, '$'
    buff    db 7,7 Dup(?)
.code


main:
    mov ax, @data
    mov ds, ax

    xor ax, ax
    xor bx, bx
    xor cx, cx
    xor dx, dx

    call InputInt
    mov bx, ax
    call InputInt
    xchg ax, bx   ;ax - 1  bx  - 2
    call divproc

    call OutInt
    mov ax, 4c00h
    int 21h

neget proc
  cmp ax, 32768
  jc ng
  ret
  ng:
  neg ax
neget endp

divproc proc
  push cx
  push dx
  xor cx, cx

  cmp ax, 32768
  jnc negat1
r1:
  cmp bx, 0
  je zerodiv
r0:
  cmp bx, 32768
  jnc negat2
r2:
  div bx
  cmp cx, 1
  je negat3
r3:
  pop dx
  pop cx
  ret
negat1:
  neg ax
  inc cx
  jmp r1

negat2:
  neg bx
  inc cx
  jmp r2
negat3:
  neg ax
  jmp r3
zerodiv:
    mov dx, offset message
    mov ah, 09h
    int 21h
    mov ax, 4c00h
    int 21h
divproc endp

OutInt proc
  push ax
  push bx
  push cx
  push dx


   test    ax, ax
   jns     oi1


   mov  cx, ax   ; смена знака
   mov     ah, 02h
   mov     dl, '-'
   int     21h
   mov  ax, cx
   neg     ax

oi1:
    xor     cx, cx
    mov     bx, 10
oi2:
    xor     dx, dx     ;запихиваем в стек
    div     bx

    push    dx
    inc     cx

    test    ax, ax
    jnz     oi2

    mov     ah, 02h
oi3:
    pop     dx        ;выводим из стека

    add     dl, '0'
    int     21h

    loop    oi3

    mov ah, 09h
    mov dx, offset endl
    int 21h

    pop dx
    pop cx
    pop bx
    pop ax

    ret

OutInt endp

InputInt proc
    push bx
    push cx
    push dx
    push di

    mov ah, 0ah
    xor di, di
    mov dx, offset buff
    int 21h
    mov dl, 0ah
    mov ah, 02h
    int 21h

    mov si, offset buff + 2  ; берём адрес 1-го символа и проверяем на минус
    cmp byte ptr [si], '-'
    jnz ii1
    mov di,1
    inc si
ii1:
    xor ax,ax
    mov bx,10
ii2:
    mov cl, [si]             ; непосредственно ввод
    cmp cl, 0dh
    jz endin

    cmp cl, '0'
    jb er
    cmp cl, '9'
    ja er

    sub cl, '0'
    mul bx
    jo er
    add ax, cx
    inc si
    jmp ii2

er:
    mov dx, offset error
    mov ah, 09h
    int 21h
    mov ax, 4c00h
    int 21h


endin:
    cmp ax, 32768
    jnc er
    cmp di, 1
    jnz ii3
    neg ax
ii3:
    pop di
    pop dx
    pop cx
    pop bx
    ret
InputInt endp




end main
