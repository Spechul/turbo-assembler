.model tiny
.stack 256
.data
    message db 'wrong input', 10, 13, '$'
    endl db ' ', 10, 13, '$'
    inpVar dw 1
    outpVar dw 1

.code
main:
    mov ax, @data
    mov ds, ax

call input
mov ax, inpVar
mov outpVar, ax
call input
xor ax, ax
;xor bx, bx
;xor cx, cx
xor dx, dx
mov ax, outpVar
mov cx, inpVar
div cx
call output


mov ah, 09h
lea dx, endl
int 21h

eof:
    mov ax, 4c00h
    int 21h
;    m:
;    mul 000Ah
;    jmp me

    input proc ; 65,535 max short
      push ax
      push bx
      push cx
      push dx
      mov inpVar, 0
      xor ax, ax
      xor dx, dx
      xor bx, bx
      xor cx, cx

    inpcycle:
      mov ah, 01h
      int 21h

      cmp al, 0dh
      je eoinput

      sub al, 30h

      cmp al, 10
      jnc fail

      cbw
      mov bx, ax
      mov ax, inpVar
      mov dx, 10
      mul dx
      jc fail
      add ax, bx

      jz fail1
zeroinput:
      mov inpVar, ax
      inc cx

      cmp cx, 5
      je maxreached
      jmp inpcycle

maxreached:
      mov ah, 09h
      lea dx, endl
      int 21h
      jmp eoinput

    fail1:
      cmp bx, 0
      jz zeroinput

    fail:
      mov ah, 09h
      lea dx, message
      int 21h
      mov ax, 4c00h
      int 21h
      mov inpVar, 1


    eoinput:

      pop dx
      pop cx
      pop bx
      pop ax

      ret
    input endp

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
output proc
  push ax
  push bx
  push cx
  push dx

    xor cx, cx
    mov bx, 10
out1:
    xor dx,dx
    div bx

    push dx
    inc cx

    test ax, ax
    jnz out1

    mov ah, 02h
out2:
    pop dx

    add dl, 30h
    int 21h

    loop out2

    pop dx
    pop cx
    pop bx
    pop ax

    ret

output endp

end main
