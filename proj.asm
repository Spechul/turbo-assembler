.model small
.stack 256
.data
    a dw 1
    b dw 2
    c dw 3
    d dw 4
    temp dw ?
.code
main:
    mov ax, @data
    mov ds, ax
;left
    mov ax, a
    mul c
    mov cx, ax
    mov ax, b
    mul d
    add ax, cx
    mov bx, ax
;right
    mov ax, a
    mul d
    mov cx, ax
    mov ax, b
    mul c
    add ax, cx
    cmp ax, bx

    jz if1

    jnz if2

if1:
  mov ax, a
  mul a

  jmp eof

if2:
  mov ax, a
  cmp ax, c

  jnc if3

  jc if4

if3:
  mov ax, c
  and ax, b

  jmp eof

if4:
  mov ax, a
  mov bx, b
  mov cx, c
  or bx, cx
  sub ax, bx

  jmp eof

eof:
    mov ax, 4c00h
    int 21h
end main
