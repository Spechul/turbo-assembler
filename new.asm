codesegm segment
assume cs:codesegm, ds:codesegm, ss:codesegm, es:codesegm
org 100h
main:
  KURWA dd 123
  flag dw 2289h

  mov ax, 1
  mov ax, 2
  mov ax, 3
  mov ax, offset main
  mov es, ax
  mov si, 8
  mov bx, word ptr es:[si]

  mov ax, 4c00h
  int 21h

codesegm ends
end main
