.model small
.stack 256
.data
  a dw 1
  b dw 2
  c dw 3
  d dw 4
  temp1 dw ?
  temp2 dw ?

.code
main:
  mov ax, @data ; into register
  mov ds, ax
;  mov ax, offset a
  mov ax, a

  mov bx, c
  mul bx
  mov temp1, ax
;2
  mov ax, b
  mov bx, d
  mul bx
;3
  mov bx, temp1
  add ax, bx
  mov temp1, ax
;right side
  mov ax, a
  mov bx, d
  mul bx
  mov temp2, ax
  mov ax, b
  mov bx, c
  mul bx
  mov bx, temp2
  add ax, bx
  mov temp2, ax
  mov bx, temp1

  cmp ax, bx
  ;
;  mov ax, 2
;  mov bx, 5
;  sub ax, bx
;  mul ax

  jz mark1
            ;elif
  jnz mark2

mark1:
  mov ax, a
  mul ax
  jmp ende

mark2:
  mov ax, a
  mov bx, c
  sub ax, bx

  jnc mark3
            ;else
  jc mark4

mark3:
  mov ax, c
  mov bx, b
  and ax, bx
  jmp ende

mark4:
  mov ax, b
  mov bx, c
  or ax, bx
  mov bx, a
  sub bx, ax
  mov ax, bx
  jmp ende


ende:
  mov ax, 4c00h
  int 21h
end main
