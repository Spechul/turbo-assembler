.model tiny
.code
org 100h
Start:
  jmp SetMyHandler

MyIntHandler proc

  StandartOffset dd ?
  flag dw 1488h
  push es ds si di cx bx dx ax
  xor ax, ax
;  mov ah, 02h
;  int 16h
  in al, 60h
  cmp al, 10h
  jne standartHandler
  call printToBuff


endHandler:
  mov al, 20h
  out 20h, al
  pop ax dx bx cx di si ds es
  iret

standartHandler:
  pop ax dx bx cx di si ds es
  jmp dword ptr cs:StandartOffset
  iret

MyIntHandler endp


SetMyHandler:
  mov ah, 35h
  mov al, 09h
  int 21h

  mov word ptr StandartOffset, bx
  mov word ptr StandartOffset + 2, es

  mov ax, 2509h
  mov bx, cs
  mov ds, bx
  mov dx, offset MyIntHandler
  int 21h

  mov dx, offset last_byte
  int 27h

printToBuff proc
  push ax es ds si di bx cx
  ;mov si, ax
  mov ax, 0040h
  mov es, ax
  mov di, word ptr es:[001Ah]
  mov bx, word ptr es:[001Ch]

  mov al, '1'
  mov byte ptr es:[di], al
  inc di
  mov word ptr es:[001Ch], di

  pop cx bx di si ds es ax
  ret
printToBuff endp

last_byte:
end Start
