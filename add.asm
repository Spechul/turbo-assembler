.model small
.stack 256
.data
    a dw 5
    len dw 15
    message db 'Hello,    ld-1$'
.code
main:
    mov ax, @data
    mov ds, ax
    mov es, ax
    xor ax, ax
    sub ax, 65535
    mov ax, offset message
    mov di, ax

    xor ax, ax
    xor bx, bx
    xor cx, cx
    xor dx, dx

    mov cx, len
    mov al, 32
repeat:
    repnz scasb
    jnz next
    inc dx
    jmp repeat
next:
    mov ax, 4c00h
    int 21h
end main
