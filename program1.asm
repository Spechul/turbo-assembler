.model small
.stack 256
.data
    a dw 5
    b dw 6
    message db 'Hello world!', 13, 10, '$'
.code
main:
    mov ax, @data
    mov ds, ax
    
    mov ax, a
    add ax, b

    mov ah, 9
    mov dx, offset message
    int 21h
    
    mov ax, 4c00h
    int 21h
end main