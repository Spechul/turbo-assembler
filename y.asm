model tiny
.code
.386
org 100h
Start:
  mov si, 80h
  lodsb
  cmp al, 0
  je SetMyHandler
  cmp al, 3
  jne InputError
  mov cx, 3
  mov di, offset param
  repe cmpsb
  jne InputError

  call RemoveHandler
  mov ah, 4ch
  int 21h

CheckInstallation proc
  push ax bx es si

  xor ax, ax
  mov es, ax

  mov si, word ptr es:[09h * 4]   ;берем адрес обработчика
  mov bx, word ptr es:[09h * 4 + 2] ;и его сегмент
  mov es, bx
  cmp word ptr es:[si + 8], 1488h
  ;jne bad
  ;cmp bx, 61440
  ;jne bad
  jne good
  bad:
  mov ah, 09h
  mov dx, offset inst_err
  int 21h
  mov ah, 4ch
  int 21h
  good:
  mov ax, cs
  mov es, ax
;  call out_uns
  mov ax, offset flag
;  call out_uns
  pop si es bx ax
  ret
CheckInstallation endp

SetMyHandler:
  push ax es
  call CheckInstallation
  xor ax, ax
  mov es, ax
  mov ax, word ptr es:[09h * 4]
  ;call out_uns
  mov word ptr OldHandler, ax
  mov ax, word ptr es:[09h * 4 + 2]
  ;call out_uns
  mov word ptr OldHandler + 2, ax

  mov ax, cs
  mov word ptr es:[09h * 4 + 2], ax
  mov ax, offset handler
  mov word ptr es:[09h * 4], ax
  pop es ax
  mov ah, 09h
  mov dx, offset good_inst
  int 21h
  mov dx, offset last_byte
  int 27h
  ret

InputError:
  mov ah, 09h
  mov dx, offset inp_err
  int 21h
  mov ah, 4ch
  int 21h
  ret

RemoveHandler proc
  push dx ax bx es si
  call CheckIfDeleted
  xor ax, ax
  mov es, ax

  mov ax, word ptr es:[09h * 4]   ;берем адрес нового обработчика
  mov bx, word ptr es:[09h * 4 + 2] ;и его сегмент
  mov es, bx
  mov si, ax
  ;xor ax, ax
  ;mov word ptr es:[si + 4], ax
  mov ax, word ptr es:[si]  ;берем первое слово по этому адресу(адрес старого обработчика)
  mov bx, word ptr es:[si + 2] ;берем второе слово по этому адресу(семент старого обработчика)
  push ax
  mov ax, bx
  pop ax
  push bx
  mov bx, 0     ;восстанавливаем адрес сегмента
  mov es, bx
  pop bx
  mov word ptr es:[09h * 4], ax   ;кладём в адрес обработкика старый адрес
  mov word ptr es:[09h * 4 + 2], bx ;кладём в адрес сегмента обработкика старый адрес
  mov ah, 09h
  mov dx, offset good_del
  int 21h

  pop si es bx ax dx
  ret
RemoveHandler endp

CheckIfDeleted proc
  push ax bx es si

  xor ax, ax
  mov es, ax

  mov si, word ptr es:[09h * 4]   ;берем адрес нового обработчика
  mov bx, word ptr es:[09h * 4 + 2] ;и его сегмент

  mov es, bx
  mov ax, es
  add si, 8
;  call out_uns
  mov ax, si
;  call out_uns
  mov ax, word ptr es:[si]
  cmp ax, 1488h
  je good1
  bad1:
  mov ah, 09h
  mov dx, offset rem_err
  int 21h
  mov ah, 4ch
  int 21h
  good1:
  pop si es bx ax
  ret
CheckIfDeleted endp

handler:
  OldHandler dd 0, 0
  flag dw 1488h
  push es ds si di cx bx dx ax
  xor ax, ax
;  mov ah, 02h
;  int 16h
  in al, 60h
  ; проверка А
  cmp al, 1Eh
  jne firstCMP
  push cx
  mov cl, '1'
  call LetterToBuf
  pop cx
  jmp end_handler
firstCMP:
  ; проверка B
  cmp al, 30h
  jne secondCMP
  push cx
  mov cl, '2'
  call LetterToBuf
  pop cx
  jmp end_handler
secondCMP:
  ; проверка C
  cmp al, 2Eh
  jne thirdCMP
  push cx
  mov cl, '3'
  call LetterToBuf
  pop cx
  jmp end_handler
thirdCMP:
  ; проверка D
  cmp al, 20h
  jne fourthCMP
  push cx
  mov cl, '4'
  call LetterToBuf
  pop cx
  jmp end_handler
fourthCMP:
  ; проверка E
  cmp al, 12h
  jne StandartHandler
  push cx
  mov cl, '5'
  call LetterToBuf
  pop cx

end_handler:
  mov al, 20h
  out 20h, al
  pop ax dx bx cx di si ds es
  iret

StandartHandler:
  pop ax dx bx cx di si ds es
  jmp dword ptr cs:OldHandler
	iret

LetterToBuf proc
push ax es ds si di bx cx
;mov si, ax
mov ax, 0040h
mov es, ax
mov di, word ptr es:[001Ah]
mov bx, word ptr es:[001Ch]

cmp di, 003ch
jne g1
mov di, 001eh
g1:
  mov al, cl
  mov byte ptr es:[di], al
  add di, 2
  mov word ptr es:[001ch], di

pop cx bx di si ds es ax
ret
LetterToBuf endp

;out_uns proc
;    push ax
;    push cx
;    push dx
;    push bx
;    xor cx, cx
;    mov bx, 10
;
;    get_number:
;      xor dx, dx
;      div bx
;      add dl, '0'
;      push dx
;      inc cx
;      test ax, ax
;      jnz get_number
;      mov ah, 02h
;
;    output:
;      pop dx
;      int 21h
;      loop output
;
;    mov ah, 09h
;    lea dx, endl
;    int 21h
;    pop bx
;    pop dx
;    pop cx
;    pop ax
;    ret
;out_uns endp

  param db ' -r'
  good_inst db 'Interrupt is overloaded', 10, 13, '$'
  good_del db 'Handler deleted', 10, 13, '$'
  rem_err db 'Handler does not exist or is on the other level', 10, 13, '$'
  inst_err db 'Handler is already installed', 10, 13, '$'
  inp_err db 'Bad command line parameter', 10, 13, '$'
  endl db 10, 13, '$'
  msg_len dw 8
last_byte:
end Start
