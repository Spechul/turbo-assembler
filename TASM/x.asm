.model tiny
.code
.386
org 100h
Start:
  mov si, 80h
  lodsb
  cmp al, 0
  je SetMyHandler
  cmp al, 3
  jne InputError
  mov cx, 3
  mov di, offset param
  repe cmpsb
  jne InputError

  call remove_handler
  mov ah, 4ch
  int 21h

  SetMyHandler:
    ;call check_handler_inst
    ;mov ah, 35h
    ;mov al, 09h
    ;int 21h

    ;mov word ptr StandartOffset, bx
    ;mov word ptr StandartOffset + 2, es

    ;mov ax, 2509h
    ;mov bx, cs
    ;mov ds, bx
    ;mov dx, offset MyIntHandler
    ;int 21h

    ;mov dx, offset last_byte
    ;int 27h

    push ax es
    call check_handler_inst
    xor ax, ax
    mov es, ax
    mov ax, word ptr es:[09h * 4]
    ;call out_uns
    mov word ptr StandartOffset, ax
    mov ax, word ptr es:[09h * 4 + 2]
    ;call out_uns
    mov word ptr StandartOffset + 2, ax

    mov ax, cs
    mov word ptr es:[09h * 4 + 2], ax
    mov ax, offset MyIntHandler
    mov word ptr es:[09h * 4], ax
    pop es ax
    mov ah, 09h
    mov dx, offset good_inst
    int 21h
    mov dx, offset last_byte
    int 27h
    ret

check_handler_inst:
      push ax bx es si

      xor ax, ax
      mov es, ax

      mov si, word ptr es:[09h * 4]   ;берем адрес обработчика
      mov bx, word ptr es:[09h * 4 + 2] ;и его сегмент
      mov es, bx
      cmp word ptr es:[si + 8], 1488h
      ;jne bad
      ;cmp bx, 61440
      ;jne bad
      jne good
      bad:
      mov ah, 09h
      mov dx, offset inst_err
      int 21h
      mov ah, 4ch
      int 21h
      good:
      mov ax, cs
      mov es, ax
      ;call out_uns
      mov ax, offset flag
      ;call out_uns
      pop si es bx ax
      ret

remove_handler:
  push dx ax bx es si
  call check_handler_del
  xor ax, ax
  mov es, ax

  mov ax, word ptr es:[09h * 4]   ;берем адрес нового обработчика
  mov bx, word ptr es:[09h * 4 + 2] ;и его сегмент
  mov es, bx
  mov si, ax
  ;xor ax, ax
  ;mov word ptr es:[si + 4], ax
  mov ax, word ptr es:[si]  ;берем первое слово по этому адресу(адрес старого обработчика)
  mov bx, word ptr es:[si + 2] ;берем второе слово по этому адресу(семент старого обработчика)
  push ax
  mov ax, bx
  pop ax
  push bx
  mov bx, 0     ;восстанавливаем адрес сегмента
  mov es, bx
  pop bx
  mov word ptr es:[09h * 4], ax   ;кидаем в адрес обработкика старый адрес
  mov word ptr es:[09h * 4 + 2], bx ;кидаем в адрес сегмента обработкика старый адрес
  mov ah, 09h
  mov dx, offset good_del
  int 21h

  pop si es bx ax dx
  ret

check_handler_del:
  push ax bx es si

  xor ax, ax
  mov es, ax

  mov si, word ptr es:[09h * 4]   ;берем адрес нового обработчика
  mov bx, word ptr es:[09h * 4 + 2] ;и его сегмент
  ;cmp ax, 59783
  ;je bad1
  ;cmp bx, 61440
  ;je bad1
  ;cmp word ptr es:[si], 2289h
  mov es, bx
  mov ax, es
  add si, 8
  ;call out_uns
  mov ax, si
  ;call out_uns
  mov ax, word ptr es:[si]
  cmp ax, 1488h
  je good1
  bad1:
  mov ah, 09h
  mov dx, offset rem_err
  int 21h
  mov ah, 4ch
  int 21h
  good1:
  pop si es bx ax
  ret

InputError:
    mov ah, 09h
    mov dx, offset inp_err
    int 21h
    mov ax, 4C00h
    int 21h



MyIntHandler:
  StandartOffset dd ?
  flag dw 1488h
  push es ds si di cx bx dx ax
  xor ax, ax
;  mov ah, 02h
;  int 16h
  in al, 60h
  ; проверка А
  cmp al, 1Eh
  jne firstCMP
  push cx
  mov cl, '1'
  call printToBuff
  pop cx
  jmp endHandler
firstCMP:
  ; проверка B
  cmp al, 30h
  jne secondCMP
  push cx
  mov cl, '2'
  call printToBuff
  pop cx
  jmp endHandler
secondCMP:
  ; проверка C
  cmp al, 2Eh
  jne thirdCMP
  push cx
  mov cl, '3'
  call printToBuff
  pop cx
  jmp endHandler
thirdCMP:
  ; проверка D
;  cmp al, 20h
;  jne fourthCMP
;  push cx
;  mov cl, '4'
;  call printToBuff
;  pop cx
;  jmp endHandler
fourthCMP:
  ; проверка E
  cmp al, 12h
  jne standartHandler
  push cx
  mov cl, '5'
  call printToBuff
  pop cx

endHandler:
  mov al, 20h
  out 20h, al
  pop ax dx bx cx di si ds es
  iret

standartHandler:
  pop ax dx bx cx di si ds es
  jmp dword ptr cs:StandartOffset
  iret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

printToBuff proc
  push ax es ds si di bx cx
  ;mov si, ax
  mov ax, 0040h
  mov es, ax
  mov di, word ptr es:[001Ah]
  mov bx, word ptr es:[001Ch]

  cmp di, 003ch
  jne g1
  mov di, 001eh
  g1:
    mov al, cl
    mov byte ptr es:[di], al
    add di, 2
    mov word ptr es:[001ch], di

  pop cx bx di si ds es ax
  ret
printToBuff endp


param db ' -r'
good_inst db 'Installation completed', 10, 13, '$'
good_del db 'Handler deleted', 10, 13, '$'
rem_err db 'Handler does not exist', 10, 13, '$'
inst_err db 'Handler is already installed', 10, 13, '$'
inp_err db 'Bad command line parameter', 10, 13, '$'

last_byte:
end Start
